﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace ReviewSabtu.Entities.Entities.PostGre
{
    public partial class SulapAppDbContext : DbContext
    {
        
        public SulapAppDbContext(DbContextOptions<SulapAppDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<TbAktivitas> TbAktivitas { get; set; }
        public virtual DbSet<TbPesulap> TbPesulap { get; set; }
        public virtual DbSet<TbSulap> TbSulap { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TbAktivitas>(entity =>
            {
                entity.Property(e => e.AktivitasId).UseIdentityAlwaysColumn();

                entity.Property(e => e.CreatedAt).HasDefaultValueSql("now()");

                entity.Property(e => e.CreatedBy).HasDefaultValueSql("'SYSTEM'::character varying");

                entity.Property(e => e.UpdatedAt).HasDefaultValueSql("now()");

                entity.Property(e => e.UpdatedBy).HasDefaultValueSql("'SYSTEM'::character varying");

                entity.HasOne(d => d.Pesulap)
                    .WithMany(p => p.TbAktivitas)
                    .HasForeignKey(d => d.PesulapId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TbAktivitas_TbPesulap");

                entity.HasOne(d => d.Sulap)
                    .WithMany(p => p.TbAktivitas)
                    .HasForeignKey(d => d.SulapId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TbAktivitas_TbSulap");
            });

            modelBuilder.Entity<TbPesulap>(entity =>
            {
                entity.Property(e => e.PesulapId).UseIdentityAlwaysColumn();
            });

            modelBuilder.Entity<TbSulap>(entity =>
            {
                entity.Property(e => e.SulapId).UseIdentityAlwaysColumn();
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
