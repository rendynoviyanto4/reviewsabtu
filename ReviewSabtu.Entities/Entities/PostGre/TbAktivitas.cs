﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ReviewSabtu.Entities.Entities.PostGre
{
    public partial class TbAktivitas
    {
        [Key]
        public int AktivitasId { get; set; }
        [Required]
        [StringLength(255)]
        public string AktivitasName { get; set; }
        public int AktivitasCount { get; set; }
        [Column(TypeName = "timestamp with time zone")]
        public DateTimeOffset AktivitasDate { get; set; }
        public int PesulapId { get; set; }
        public int SulapId { get; set; }
        [Column(TypeName = "timestamp with time zone")]
        public DateTime CreatedAt { get; set; }
        [Required]
        [StringLength(255)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "timestamp with time zone")]
        public DateTime UpdatedAt { get; set; }
        [Required]
        [StringLength(255)]
        public string UpdatedBy { get; set; }

        [ForeignKey(nameof(PesulapId))]
        [InverseProperty(nameof(TbPesulap.TbAktivitas))]
        public virtual TbPesulap Pesulap { get; set; }
        [ForeignKey(nameof(SulapId))]
        [InverseProperty(nameof(TbSulap.TbAktivitas))]
        public virtual TbSulap Sulap { get; set; }
    }
}
