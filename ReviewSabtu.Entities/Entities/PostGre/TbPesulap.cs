﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ReviewSabtu.Entities.Entities.PostGre
{
    public partial class TbPesulap
    {
        public TbPesulap()
        {
            TbAktivitas = new HashSet<TbAktivitas>();
        }

        [Key]
        public int PesulapId { get; set; }
        [Required]
        [StringLength(255)]
        public string PesulapName { get; set; }

        [InverseProperty("Pesulap")]
        public virtual ICollection<TbAktivitas> TbAktivitas { get; set; }
    }
}
