﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ReviewSabtu.Entities.Entities.PostGre
{
    public partial class TbSulap
    {
        public TbSulap()
        {
            TbAktivitas = new HashSet<TbAktivitas>();
        }

        [Key]
        public int SulapId { get; set; }
        [Required]
        [StringLength(255)]
        public string SulapName { get; set; }

        [InverseProperty("Sulap")]
        public virtual ICollection<TbAktivitas> TbAktivitas { get; set; }
    }
}
