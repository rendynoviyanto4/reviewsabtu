SELECT a."AktivitasId", ps."PesulapName", s."SulapName",
       a."AktivitasDate", a."AktivitasCount"
FROM "TbAktivitas" a
JOIN "TbPesulap" ps
ON a."PesulapId" = ps."PesulapId"
JOIN "TbSulap" s
ON s."SulapId" = a."SulapId"